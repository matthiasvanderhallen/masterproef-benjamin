-- The theory generator generates an idp theory following the third implementation

local generator = { }
local debug = 0

-- Generate an idp theory for the given cudf interpretation by filling in the given
-- template file. The output is written to a file at the given output path
function generator.generate( cudf_interpretation, template_file, output_file )
    local lupa = require("lupa")
    local result = lupa.expand_file(template_file, get_data(cudf_interpretation))

    local file = io.open(output_file, "w")
    file:write(result)
    io.close(file)
end

-- Get the data needed to fill in the template
-- data: { packages, names, version, package_dependencies, virtual_provides, 
--         dependencies, conflicts, requests }
function get_data( cudf_interpretation )
    data = { }

    packages = get_packages(cudf_interpretation)
    virtual_packages = nil

    data.packages = packages
    data.names = get_package_names(cudf_interpretation)
    data.version = get_version(cudf_interpretation)
    data.package_dependencies = get_package_dependencies(cudf_interpretation)
    data.virtual_provides = get_provides_list(cudf_interpretation, data.version)

    local package_names = { }
    for _, v in pairs(data.names) do
        package_names[v] = true
    end

    data.dependencies = get_dependencies(cudf_interpretation, package_names)
    data.conflicts = get_conflicts(cudf_interpretation, package_names)
    data.requests = get_requests(cudf_interpretation)

    return data
end

-- Get the set of packages
-- data: { {package, name, version, installed, keep_type }, ... }
function get_packages( cudf_interpretation )
    local packages = { }
    for _, pkg in pairs(cudf_interpretation["package"]) do
        table.insert(packages, {
            package = get_package_representation(pkg),
            name = cleanup_string(pkg["package"]),
            version = pkg["version"],
            installed = pkg["installed"],
            keep_type = pkg["keep"]
        })
    end
    return packages
end

-- Get the set of package names
-- data: { ... }
function get_package_names( cudf_interpretation )
    local names = { }

    for _, pkg in pairs(cudf_interpretation["package"]) do
        table.insert(names, cleanup_string(pkg["package"]))
        for _, virt in pairs(pkg["provides"]) do
            table.insert(names, cleanup_string(virt["name"]))
        end
        for _, dep in pairs(pkg["depends"]) do
            if dep ~= "TrueDep()" and dep ~= "FalseDep()" then
                while dep["constraint"] ~= "EmptyOrDep()" do
                    local name = cleanup_string(dep["constraint"]["name"])
                    table.insert(names, name)
                    dep = dep["next_dep"]
                end
            end
        end
        for _, conf in pairs(pkg["conflicts"]) do
            local name = cleanup_string(conf["name"])
            table.insert(names, name)
        end
    end

    return remove_duplicates(names)
end

-- Get the minimal and maximal version
-- data: { min, max }
function get_version( cudf_interpretation )
    local first_key, first_val = next(cudf_interpretation["package"])
    if first_key == nil then
        return { min = 0, max = 0 }
    end
    local min = first_val["version"]
    local max = first_val["version"]


    for _, pkg in pairs(cudf_interpretation["package"]) do
        if pkg["version"] ~= nil then
            min = math.min(min, pkg["version"])
            max = math.max(max, pkg["version"])
        end

        for _, pvc in pairs(pkg["constraints"]) do
            if pvc["version"] ~= nil then
                min = math.min(min, pvc["version"])
                max = math.max(max, pvc["version"])
            end
        end
    end

    if cudf_interpretation["request"][1] ~= nil then
        for _, pvc in pairs(cudf_interpretation["request"][1]["constraints"]) do
            if pvc["version"] ~= nil then
                min = math.min(min, pvc["version"])
                max = math.max(max, pvc["version"])
            end
        end
    end

    return { min = min, max = max+1 }
end

-- Get the set of package dependencies
-- data: { ... }
function get_package_dependencies( cudf_interpretation )
    local dependencies = { }

    for _, pkg in pairs(cudf_interpretation["package"]) do
        for _, dep in pairs(pkg["depends"]) do
            if dep ~= "TrueDep()" and dep ~= "FalseDep()" then
                table.insert(dependencies, get_package_dependency_representation(pkg, dep))
            end
        end
    end

    return dependencies
end

-- Get the set of provided packages
-- data: { { package, name, version }, ... }
function get_provides_list( cudf_interpretation, versions )
    local provided = { }

    for _, pkg in pairs(cudf_interpretation["package"]) do
        -- table.insert(provided, { 
        --     package = get_package_representation(pkg),
        --     name = cleanup_string(pkg["package"]),
        --     version = pkg["version"]
        -- })
        for _, virt in pairs(pkg["provides"]) do
            if virt["version"] ~= nil then
                table.insert(provided, { 
                    package = get_package_representation(pkg),
                    name = cleanup_string(virt["name"]),
                    version = virt["version"]
                })
            else 
                for i=versions.min,versions.max do
                    table.insert(provided, { 
                        package = get_package_representation(pkg),
                        name = cleanup_string(virt["name"]),
                        version = i
                    })
                end
            end
        end
    end

    return provided
end

-- Get the set of dependencies
-- data: { { package_dep, package, 
--           dependencies = { { name, version, rel }, ... } }, ... }
function get_dependencies( cudf_interpretation, package_names )
    -- package, package_dependency, (name, version, rel)
    local dependencies = { }

    for _, pkg in pairs(cudf_interpretation["package"]) do
        for _, dep in pairs(pkg["depends"]) do
            if dep ~= "TrueDep()" and dep ~= "FalseDep()" then
                local list = { }
                local package_dep = get_package_dependency_representation(pkg, dep)
                while dep["constraint"] ~= "EmptyOrDep()" do
                    local name = cleanup_string(dep["constraint"]["name"])
                    table.insert(list, { 
                        name = name,
                        version = dep["constraint"]["version"],
                        rel = get_relation_representation(dep["constraint"]["rel"])
                    })
                    dep = dep["next_dep"]
                end
                
                if next(list) ~= nil then
                    table.insert(dependencies, {
                        package_dependency = package_dep,
                        package = get_package_representation(pkg),
                        dependencies = list
                    })
                end
            end
        end
    end

    return dependencies
end

-- Get the set of conflicts
-- data: { { package, name, rel, version }, ... }
function get_conflicts( cudf_interpretation, package_names )
    local conflicts = { }

    for _, pkg in pairs(cudf_interpretation["package"]) do
        for _, conf in pairs(pkg["conflicts"]) do
            local name = cleanup_string(conf["name"])
            table.insert(conflicts, {
                package = get_package_representation(pkg),
                name = name,
                rel = get_relation_representation(conf["rel"]),
                version = conf["version"]
            })
        end
    end

    return conflicts
end

-- Get the set of requests
-- data: { install = { { name, version, rel }, ... }
--         remove  = { { name, version, rel }, ... }
--         upgrade = { { name, version, rel }, ... } }
function get_requests( cudf_interpretation )
    local requests = {
        install = { },
        remove = { },
        upgrade = { }
    }
    
    for k, _ in pairs(requests) do
        if cudf_interpretation["request"][1] ~= nil then
            for _, v in pairs(cudf_interpretation["request"][1][k]) do
                table.insert(requests[k], {
                    name = cleanup_string(v["name"]),
                    version = v["version"],
                    rel = get_relation_representation(v["rel"])    
                })
            end
        end
    end

    return requests
end

-- Remove the duplicate values of the given table
function remove_duplicates( tab )
    local new_tab = { }
    local hash = { }

    for _, v in pairs(tab) do
        if hash[v] == nil then
            hash[v] = true
            table.insert(new_tab, v)
        end
    end

    return new_tab
end

-- Clean up a string for usage in IDP
function cleanup_string( str )
    local remove_class = "[-%.%+%%!@#$%^&%*%(%)=%[%]{};:%'\"\\|/%?<>,]"
    local result, _ = string.gsub(str, remove_class, "_")
    if not string.match(result, "^%a") then
        result = "A" .. result
    end
    return result
end

-- Get the package representation
function get_package_representation( pkg )
    return cleanup_string(pkg["package"]) .. "_v" .. pkg["version"]
end

-- Get the package dependency represenation
function get_package_dependency_representation( pkg, dep )
    local result = get_package_representation(pkg) 
    result = result .. "_" .. get_dependency_representation(dep["constraint"])
    dep = dep["next_dep"]
    while dep["constraint"] ~= "EmptyOrDep()" do
        result = result .. "_OR_" .. get_dependency_representation(dep["constraint"])
        dep = dep["next_dep"]
    end
    return result
end

-- Get the dependency represenation
function get_dependency_representation( pvc )
    if pvc["rel"] == nil or pvc["version" == nil] then
        return "dep_" .. cleanup_string(pvc["name"])
    else 
        return "dep_" .. cleanup_string(pvc["name"]) .. "_" .. pvc["rel"]:gsub('[%(%)]','') .. "_" .. pvc["version"]
    end
end

-- Get the relation representation
function get_relation_representation( rel )
    if  rel == "EQ()" then
        return "="
    elseif rel == "NEQ()" then
        return "~="
    elseif rel == "GT()" then
        return ">"
    elseif rel == "GE()" then
        return ">="
    elseif rel == "LT()" then
        return "<"
    elseif rel == "LE()" then
        return "=<"
    else 
        return nil
    end
end


return generator
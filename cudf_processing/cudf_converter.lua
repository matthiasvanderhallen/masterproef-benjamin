-- Convert the output from the CUDF parser to an interpretation that can be
-- used for the second implementation of the dependency resolver

local cudf_converter = { }
local debug = 0

local preprocessor = require("cudf_preprocessor")
local provide_list = { }
local package_cache = { }

-- Convert the given CUDF interpretation to an interpretation that can be used
-- by the second implementation of the dependency resolver
function cudf_converter.convert( cudf_interpretation )
    preprocessor = require("cudf_preprocessor")
    provide_list = preprocessor.get_provide_list(cudf_interpretation)
    package_cache = preprocessor.get_package_cache(cudf_interpretation)
    
    local result = { }
    if debug > 0 then io.stderr:write(">>> Converter: preamble\n") end
    result["preamble"] = cudf_interpretation["preamble"]
    if debug > 0 then io.stderr:write(">>> Converter: package\n") end
    result["package"] = convert_packages(cudf_interpretation["package"])
    if debug > 0 then io.stderr:write(">>> Converter: request\n") end
    result["request"] = convert_requests(cudf_interpretation["request"])
    return result
end

-- Convert a package to a versioned package
function convert_to_versioned_package( package )
    return { package = package["package"], version = package["version"] }
end

-- Convert a package version constraint to a list of packages in the repository
-- that fulfil the constraint
function convert_package_version_constraint( pvc )
    local valid_packages = preprocessor.get_valid_packages(pvc, package_cache, provide_list)
    local result = { }
    for k, v in pairs(valid_packages) do
        table.insert(result, convert_to_versioned_package(v))
    end
    return result
end

-- Convert an or dependency to a list of packages in the repository
-- that fulfil the dependency
function convert_or_dependency( dep )
    local valid_packages = { }
    while dep["constraint"] ~= "EmptyOrDep()" do
        valid_packages = insert_all(valid_packages, convert_package_version_constraint(dep["constraint"]))
        dep = dep["next_dep"]
    end
    return valid_packages
end

-- Convert a list of dependencies to a list of packages in the repository
-- that fulfil the dependency, for each dependency
function convert_dependencies( dep )
    local result = { }
    for k, v in pairs(dep) do
        if v ~= "TrueDep()" and v ~= "FalseDep()" then
            table.insert(result, convert_or_dependency(v))
        end
    end
    return result
end

-- Convert a list of PVCs to a list of packages in the repository
-- that fulfil the constraint, for each PVC
function convert_constraints( pvcs )
    local result = { }
    for k, v in pairs(pvcs) do
        table.insert(result, convert_package_version_constraint(v))
    end
    return result
end

-- Convert a package and all its data
function convert_package( package )
    local result = { }
    result["package"] = package["package"]
    result["version"] = package["version"]
    result["depends"] = convert_dependencies(package["depends"])
    result["installed"] = package["installed"]
    result["provides"] = package["provides"]
    result["conflicts"] = flatten_list(convert_constraints(package["conflicts"]))
    result["keep"] = package["keep"]
    return result
end

-- Converts a list of packages
function convert_packages( packages )
    local result = { }
    for k, v in pairs(packages) do
        table.insert(result, convert_package(v))
    end
    return result
end

-- Convert a user request
function convert_requests( request )
    local result = { }
    result["remove"] = convert_constraints(request[1]["remove"])
    result["upgrade"] = convert_constraints(request[1]["upgrade"])
    result["install"] = convert_constraints(request[1]["install"])
    return { result }
end

-- Insert all elements of the given set in the table
function insert_all( tab, set )
    for k, v in pairs(set) do
        table.insert(tab, v)
    end
    return tab
end

-- Flatten a list
function flatten_list( list )
    local result = { }
    for k, v in pairs(list) do
        result = insert_all(result, v)
    end
    return result
end

return cudf_converter
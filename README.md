# Masterproef implementatie

## Data voor experimenten
De data is beschikbaar op: http://data.mancoosi.org/

Een overzicht van de resultaten van andere solvers is ook beschikbaar op: http://www.mancoosi.org/misc-2010/results/paranoid/

### Tip
Je kan de data downloaden met *wget*.
Bijvoorbeeld: 
```bash
$ wget --recursive --no-parent --show-progress http://data.mancoosi.org/misc2010/results/problems/cudf_set/
$ find ./ *.bz2 -type f -exec bzip2 -d {} \;
```

## Code uitvoeren
### Solvers runnen: *./run_misc.py*
```bash
$ python run_misc.py -h
Run the tests on cudf data. Arguments:
    -h: show this help message
    -d: directory with cudf files   default: ./
    -p: number of processes to use  default: 1
    -o: output directory            default: cudf_output/
    -f: output file path            default: cudf_output/output.txt
    -m: memory limit (MB)           default: -1 (unlimited)
    -t: time limit (s)              default: -1 (unlimited)
    -v: version to use (1-3)        default: 1
```

### Output parsen: *cudf_output/process_cudf_output.py*
```bash
$ python process_cudf_output.py -h
Process the cudf test output. Arguments:
    -h: show this help message
    -o: write the result to this csv file   default: output.csv
    -d: cudf output directory               default: ./
    -c: cudf problem directory              default: ./
    -r: recursive
```

### Plots
Zie R code in *./plots*
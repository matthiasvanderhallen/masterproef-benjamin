#!/bin/bash
# Execute the test_file(filename) method in test_cudf.idp and save the results to a file

IDP=/home/benjamin/Apps/idp/bin/idp
$IDP -e "test_file(\"$1\")" test_cudf.idp 2>&1 | tee "output_dependency_resolver/$(basename $1).out"
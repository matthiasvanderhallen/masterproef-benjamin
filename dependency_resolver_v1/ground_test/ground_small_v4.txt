>>> Start
>>> Parsing
>>> [Parsing took 0.046809s]
>>> Adding stanzas
	Stanzas ( 1/11): 600 packages added
	Stanzas ( 2/11): 600 versions added
	Stanzas ( 3/11): 600 versioned packages added
	Stanzas ( 4/11): 600 installations marked
	Stanzas ( 5/11): 3619 package version constraints added
	Stanzas ( 6/11): 2442 orDependencies added
	Stanzas ( 7/11): 990 dependencies added
before ct
after ct
after pt
	Stanzas ( 8/11): 990 dependencies marked
	Stanzas ( 9/11): 2348 conflicts marked
	Stanzas (10/11): 0 provides marked
	Stanzas (11/11): 600 keep marked
>>> [Adding stanzas took 0.529331s]
>>> Cleaning structure
>>> Solving
Starting definition evaluation at 604ms
Starting approximation at 173879ms
Applying propagation to structure
Creating grounders at 179878ms
Starting grounding at 180130ms
Rule isFulfilledDependency(TrueDep) <- (Dependency(TrueDep)). was grounded to 0 atoms.
Rule isFulfilledDependency(FalseDep) <- false. was grounded to 0 atoms.
Rule ! orDep[OrDependency] : isFulfilledDependency(dependency(orDep[OrDependency])) <- (isFulfilledOrDependency(orDep[OrDependency]) & Dependency(dependency(orDep[OrDependency]))). was grounded to 1186 atoms.
Rule isFulfilledOrDependency(EmptyOrDep) <- false. was grounded to 0 atoms.
Rule ! orDep[OrDependency] pvc[PackageVersionConstraint] : isFulfilledOrDependency(orDependency(pvc[PackageVersionConstraint],orDep[OrDependency])) <- ((isFulfilledConstraint(pvc[PackageVersionConstraint]) | isFulfilledOrDependency(orDep[OrDependency])) & OrDependency(orDependency(pvc[PackageVersionConstraint],orDep[OrDependency]))). was grounded to 1420828 atoms.
Rule ! pvc[PackageVersionConstraint] : isFulfilledConstraint(pvc[PackageVersionConstraint]) <- (? vp[VersionedPackage] : isFulfilledConstraintBy(pvc[PackageVersionConstraint],vp[VersionedPackage])). was grounded to 360000 atoms.
Rule ! pvc[PackageVersionConstraint] vp[VersionedPackage] : isFulfilledConstraintBy(pvc[PackageVersionConstraint],vp[VersionedPackage]) <- ((isInstalledAfter(vp[VersionedPackage]) & versionedPackageSatisfiesConstraint(vp[VersionedPackage],pvc[PackageVersionConstraint])) | isFulfilledProvidesBy(pvc[PackageVersionConstraint],vp[VersionedPackage])). was grounded to 360000 atoms.
Rule ! pvc[PackageVersionConstraint] : isFulfilledProvides(pvc[PackageVersionConstraint]) <- (? vp[VersionedPackage] : isFulfilledProvidesBy(pvc[PackageVersionConstraint],vp[VersionedPackage])). was grounded to 0 atoms.
Rule ! pvc[PackageVersionConstraint] vp[VersionedPackage] : isFulfilledProvidesBy(pvc[PackageVersionConstraint],vp[VersionedPackage]) <- (? virtpkg[VirtualPackage] : (isInstalledAfter(vp[VersionedPackage]) & provides(vp[VersionedPackage],virtpkg[VirtualPackage]) & virtualPackageSatisfiesConstraint(virtpkg[VirtualPackage],pvc[PackageVersionConstraint]))). was grounded to 0 atoms.
Rule ! vp[VersionedPackage] : isRemoved(vp[VersionedPackage]) <- (isInstalledBefore(vp[VersionedPackage]) & ~isInstalledAfter(vp[VersionedPackage]) & (! vp2[VersionedPackage] : ~isUpgraded(vp2[VersionedPackage],vp[VersionedPackage]))). was grounded to 0 atoms.
Rule ! vp[VersionedPackage] : isNewlyInstalled(vp[VersionedPackage]) <- (~isInstalledBefore(vp[VersionedPackage]) & isInstalledAfter(vp[VersionedPackage]) & (! vp2[VersionedPackage] : ~isUpgraded(vp2[VersionedPackage],vp[VersionedPackage]))). was grounded to 130300 atoms.
Rule ! package[PackageName] v1[VersionNumber] v2[VersionNumber] : isUpgraded(versionedPackage(package[PackageName],v2[VersionNumber]),versionedPackage(package[PackageName],v1[VersionNumber])) <- (isInstalledAfter(versionedPackage(package[PackageName],v1[VersionNumber])) & isInstalledBefore(versionedPackage(package[PackageName],v2[VersionNumber])) & >(v1[VersionNumber],v2[VersionNumber]) & VersionedPackage(versionedPackage(package[PackageName],v2[VersionNumber])) & VersionedPackage(versionedPackage(package[PackageName],v1[VersionNumber]))). was grounded to 300000 atoms.
Rule ! package[PackageName] v1[VersionNumber] : isNotUpToDate(versionedPackage(package[PackageName],v1[VersionNumber])) <- (isInstalledAfter(versionedPackage(package[PackageName],v1[VersionNumber])) & (? var208[VersionNumber] : (VersionedPackage(versionedPackage(package[PackageName],var208[VersionNumber])) & >(var208[VersionNumber],v1[VersionNumber]))) & VersionedPackage(versionedPackage(package[PackageName],v1[VersionNumber]))). was grounded to 600 atoms.
Error: std::bad_alloc

>>> Start
>>> Parsing
>>> [Parsing took 0.041441s]
>>> Adding stanzas
	Stanzas ( 1/11): 600 packages added
	Stanzas ( 2/11): 600 versions added
	Stanzas ( 3/11): 600 versioned packages added
	Stanzas ( 4/11): 600 installations marked
	Stanzas ( 5/11): 3619 package version constraints added
	Stanzas ( 6/11): 2442 orDependencies added
	Stanzas ( 7/11): 990 dependencies added
before ct
after ct
after pt
	Stanzas ( 8/11): 990 dependencies marked
	Stanzas ( 9/11): 2348 conflicts marked
	Stanzas (10/11): 0 provides marked
	Stanzas (11/11): 600 keep marked
>>> [Adding stanzas took 0.540655s]
>>> Cleaning structure
>>> Solving
Starting definition evaluation at 601ms
Starting approximation at 10146ms
Applying propagation to structure
Creating grounders at 17466ms
Starting grounding at 18999ms

Rule isFulfilledDependency(TrueDep) <- (Dependency(TrueDep)). was grounded to 0 atoms.

Rule isFulfilledDependency(FalseDep) <- false. was grounded to 0 atoms.

Rule ! orDep[OrDependency] : isFulfilledDependency(dependency(orDep[OrDependency])) <- (isFulfilledOrDependency(orDep[OrDependency]) & Dependency(dependency(orDep[OrDependency]))). was grounded to 1186 atoms.

Rule isFulfilledOrDependency(EmptyOrDep) <- false. was grounded to 0 atoms.

Rule ! orDep[OrDependency] pvc[PackageVersionConstraint] : isFulfilledOrDependency(orDependency(pvc[PackageVersionConstraint],orDep[OrDependency])) <- ((isFulfilledConstraint(pvc[PackageVersionConstraint]) | isFulfilledOrDependency(orDep[OrDependency])) & OrDependency(orDependency(pvc[PackageVersionConstraint],orDep[OrDependency]))). was grounded to 1420828 atoms.

Rule ! pvc[PackageVersionConstraint] : isFulfilledConstraint(pvc[PackageVersionConstraint]) <- (? vp[VersionedPackage] : isFulfilledConstraintBy(pvc[PackageVersionConstraint],vp[VersionedPackage])). was grounded to 360000 atoms.

Rule ! pkg[PackageName] vp[VersionedPackage] : isFulfilledConstraintBy(packageVersionConstraint(pkg[PackageName]),vp[VersionedPackage]) <- (((isInstalledAfter(vp[VersionedPackage]) & (1  =  card[ { v[VersionNumber] : =(vp[VersionedPackage],versionedPackage(pkg[PackageName],v[VersionNumber])) : 1 } ])) | isFulfilledProvidesBy(packageVersionConstraint(pkg[PackageName]),vp[VersionedPackage])) & PackageVersionConstraint(packageVersionConstraint(pkg[PackageName]))). was grounded to 1200 atoms.

Rule ! pkg[PackageName] rel[RelationOperator] v[VersionNumber] vp[VersionedPackage] : isFulfilledConstraintBy(packageVersionConstraint(pkg[PackageName],rel[RelationOperator],v[VersionNumber]),vp[VersionedPackage]) <- (((isInstalledAfter(vp[VersionedPackage]) & (1  =  card[ { v2[VersionNumber] : (=(vp[VersionedPackage],versionedPackage(pkg[PackageName],v2[VersionNumber])) & relationHolds(v2[VersionNumber],rel[RelationOperator],v[VersionNumber])) : 1 } ])) | isFulfilledProvidesBy(packageVersionConstraint(pkg[PackageName],rel[RelationOperator],v[VersionNumber]),vp[VersionedPackage])) & PackageVersionConstraint(packageVersionConstraint(pkg[PackageName],rel[RelationOperator],v[VersionNumber]))). was grounded to 3960000 atoms.

Rule ! pvc[PackageVersionConstraint] : isFulfilledProvides(pvc[PackageVersionConstraint]) <- (? vp[VersionedPackage] : isFulfilledProvidesBy(pvc[PackageVersionConstraint],vp[VersionedPackage])). was grounded to 360000 atoms.

Rule ! pkg[PackageName] vp[VersionedPackage] : isFulfilledProvidesBy(packageVersionConstraint(pkg[PackageName]),vp[VersionedPackage]) <- (isInstalledAfter(vp[VersionedPackage]) & (provides(vp[VersionedPackage],virtualPackage(pkg[PackageName])) | (? v[VersionNumber] : provides(vp[VersionedPackage],virtualPackage(pkg[PackageName],v[VersionNumber])))) & PackageVersionConstraint(packageVersionConstraint(pkg[PackageName]))). was grounded to 1200 atoms.

Rule ! pkg[PackageName] rel[RelationOperator] v[VersionNumber] vp[VersionedPackage] : isFulfilledProvidesBy(packageVersionConstraint(pkg[PackageName],rel[RelationOperator],v[VersionNumber]),vp[VersionedPackage]) <- (isInstalledAfter(vp[VersionedPackage]) & (provides(vp[VersionedPackage],virtualPackage(pkg[PackageName])) | (? v2[VersionNumber] : (relationHolds(v2[VersionNumber],rel[RelationOperator],v[VersionNumber]) & provides(vp[VersionedPackage],virtualPackage(pkg[PackageName],v2[VersionNumber]))))) & PackageVersionConstraint(packageVersionConstraint(pkg[PackageName],rel[RelationOperator],v[VersionNumber]))). was grounded to 3600000 atoms.

Rule ! vp[VersionedPackage] : isRemoved(vp[VersionedPackage]) <- (isInstalledBefore(vp[VersionedPackage]) & ~isInstalledAfter(vp[VersionedPackage]) & (! vp2[VersionedPackage] : ~isUpgraded(vp2[VersionedPackage],vp[VersionedPackage]))). was grounded to 0 atoms.

Rule ! vp[VersionedPackage] : isNewlyInstalled(vp[VersionedPackage]) <- (~isInstalledBefore(vp[VersionedPackage]) & isInstalledAfter(vp[VersionedPackage]) & (! vp2[VersionedPackage] : ~isUpgraded(vp2[VersionedPackage],vp[VersionedPackage]))). was grounded to 130300 atoms.

Rule ! package[PackageName] v1[VersionNumber] v2[VersionNumber] : isUpgraded(versionedPackage(package[PackageName],v2[VersionNumber]),versionedPackage(package[PackageName],v1[VersionNumber])) <- (isInstalledAfter(versionedPackage(package[PackageName],v1[VersionNumber])) & isInstalledBefore(versionedPackage(package[PackageName],v2[VersionNumber])) & >(v1[VersionNumber],v2[VersionNumber]) & VersionedPackage(versionedPackage(package[PackageName],v2[VersionNumber])) & VersionedPackage(versionedPackage(package[PackageName],v1[VersionNumber]))). was grounded to 300000 atoms.

Rule ! package[PackageName] v1[VersionNumber] : isNotUpToDate(versionedPackage(package[PackageName],v1[VersionNumber])) <- (isInstalledAfter(versionedPackage(package[PackageName],v1[VersionNumber])) & (? var140[VersionNumber] : (VersionedPackage(versionedPackage(package[PackageName],var140[VersionNumber])) & >(var140[VersionNumber],v1[VersionNumber]))) & VersionedPackage(versionedPackage(package[PackageName],v1[VersionNumber]))). was grounded to 600 atoms.

Error: std::bad_alloc

>>> Start
>>> Parsing
>>> [Parsing took 0.000423s]
>>> Adding stanzas
	Stanzas ( 1/11): 15 packages added
	Stanzas ( 2/11): 15 versions added
	Stanzas ( 3/11): 15 versioned packages added
	Stanzas ( 4/11): 15 installations marked
	Stanzas ( 5/11): 9 package version constraints added
	Stanzas ( 6/11): 8 orDependencies added
	Stanzas ( 7/11): 17 dependencies added
before ct
after ct
after pt
	Stanzas ( 8/11): 17 dependencies marked
	Stanzas ( 9/11): 1 conflicts marked
	Stanzas (10/11): 0 provides marked
	Stanzas (11/11): 15 keep marked
>>> [Adding stanzas took 0.005502s]
>>> Cleaning structure
>>> Solving
Starting definition evaluation at 21ms
Starting approximation at 162ms
Applying propagation to structure
Creating grounders at 233ms
Starting grounding at 332ms
Rule isFulfilledDependency(TrueDep) <- (Dependency(TrueDep)). was grounded to 0 atoms.
Rule isFulfilledDependency(FalseDep) <- false. was grounded to 0 atoms.
Rule ! orDep[OrDependency] : isFulfilledDependency(dependency(orDep[OrDependency])) <- (isFulfilledOrDependency(orDep[OrDependency]) & Dependency(dependency(orDep[OrDependency]))). was grounded to 9 atoms.
Rule isFulfilledOrDependency(EmptyOrDep) <- false. was grounded to 0 atoms.
Rule ! orDep[OrDependency] pvc[PackageVersionConstraint] : isFulfilledOrDependency(orDependency(pvc[PackageVersionConstraint],orDep[OrDependency])) <- ((isFulfilledConstraint(pvc[PackageVersionConstraint]) | isFulfilledOrDependency(orDep[OrDependency])) & OrDependency(orDependency(pvc[PackageVersionConstraint],orDep[OrDependency]))). was grounded to 162 atoms.
Rule ! pvc[PackageVersionConstraint] : isFulfilledConstraint(pvc[PackageVersionConstraint]) <- (? vp[VersionedPackage] : isFulfilledConstraintBy(pvc[PackageVersionConstraint],vp[VersionedPackage])). was grounded to 150 atoms.
Rule ! pkg[PackageName] vp[VersionedPackage] : isFulfilledConstraintBy(packageVersionConstraint(pkg[PackageName]),vp[VersionedPackage]) <- ((isFulfilledProvidesBy(packageVersionConstraint(pkg[PackageName]),vp[VersionedPackage]) | (? v[VersionNumber] : (isInstalledAfter(vp[VersionedPackage]) & =(vp[VersionedPackage],versionedPackage(pkg[PackageName],v[VersionNumber]))))) & PackageVersionConstraint(packageVersionConstraint(pkg[PackageName]))). was grounded to 120 atoms.
Rule ! pkg[PackageName] rel[RelationOperator] v[VersionNumber] vp[VersionedPackage] : isFulfilledConstraintBy(packageVersionConstraint(pkg[PackageName],rel[RelationOperator],v[VersionNumber]),vp[VersionedPackage]) <- ((isFulfilledProvidesBy(packageVersionConstraint(pkg[PackageName],rel[RelationOperator],v[VersionNumber]),vp[VersionedPackage]) | (? v2[VersionNumber] : (isInstalledAfter(vp[VersionedPackage]) & =(vp[VersionedPackage],versionedPackage(pkg[PackageName],v2[VersionNumber])) & relationHolds(v2[VersionNumber],rel[RelationOperator],v[VersionNumber])))) & PackageVersionConstraint(packageVersionConstraint(pkg[PackageName],rel[RelationOperator],v[VersionNumber]))). was grounded to 405 atoms.
Rule ! pvc[PackageVersionConstraint] : isFulfilledProvides(pvc[PackageVersionConstraint]) <- (? vp[VersionedPackage] : isFulfilledProvidesBy(pvc[PackageVersionConstraint],vp[VersionedPackage])). was grounded to 150 atoms.
Rule ! pkg[PackageName] vp[VersionedPackage] : isFulfilledProvidesBy(packageVersionConstraint(pkg[PackageName]),vp[VersionedPackage]) <- (isInstalledAfter(vp[VersionedPackage]) & (provides(vp[VersionedPackage],virtualPackage(pkg[PackageName])) | (? v[VersionNumber] : provides(vp[VersionedPackage],virtualPackage(pkg[PackageName],v[VersionNumber])))) & PackageVersionConstraint(packageVersionConstraint(pkg[PackageName]))). was grounded to 120 atoms.
Rule ! pkg[PackageName] rel[RelationOperator] v[VersionNumber] vp[VersionedPackage] : isFulfilledProvidesBy(packageVersionConstraint(pkg[PackageName],rel[RelationOperator],v[VersionNumber]),vp[VersionedPackage]) <- (isInstalledAfter(vp[VersionedPackage]) & (provides(vp[VersionedPackage],virtualPackage(pkg[PackageName])) | (? v2[VersionNumber] : (relationHolds(v2[VersionNumber],rel[RelationOperator],v[VersionNumber]) & provides(vp[VersionedPackage],virtualPackage(pkg[PackageName],v2[VersionNumber]))))) & PackageVersionConstraint(packageVersionConstraint(pkg[PackageName],rel[RelationOperator],v[VersionNumber]))). was grounded to 2880 atoms.
Rule ! vp[VersionedPackage] : isRemoved(vp[VersionedPackage]) <- (isInstalledBefore(vp[VersionedPackage]) & ~isInstalledAfter(vp[VersionedPackage]) & (! vp2[VersionedPackage] : ~isUpgraded(vp2[VersionedPackage],vp[VersionedPackage]))). was grounded to 0 atoms.
Rule ! vp[VersionedPackage] : isNewlyInstalled(vp[VersionedPackage]) <- (~isInstalledBefore(vp[VersionedPackage]) & isInstalledAfter(vp[VersionedPackage]) & (! vp2[VersionedPackage] : ~isUpgraded(vp2[VersionedPackage],vp[VersionedPackage]))). was grounded to 23 atoms.
Rule ! package[PackageName] v1[VersionNumber] v2[VersionNumber] : isUpgraded(versionedPackage(package[PackageName],v2[VersionNumber]),versionedPackage(package[PackageName],v1[VersionNumber])) <- (isInstalledAfter(versionedPackage(package[PackageName],v1[VersionNumber])) & isInstalledBefore(versionedPackage(package[PackageName],v2[VersionNumber])) & >(v1[VersionNumber],v2[VersionNumber]) & VersionedPackage(versionedPackage(package[PackageName],v2[VersionNumber])) & VersionedPackage(versionedPackage(package[PackageName],v1[VersionNumber]))). was grounded to 60 atoms.
Rule ! package[PackageName] v1[VersionNumber] : isNotUpToDate(versionedPackage(package[PackageName],v1[VersionNumber])) <- (isInstalledAfter(versionedPackage(package[PackageName],v1[VersionNumber])) & (? var145[VersionNumber] : (VersionedPackage(versionedPackage(package[PackageName],var145[VersionNumber])) & >(var145[VersionNumber],v1[VersionNumber]))) & VersionedPackage(versionedPackage(package[PackageName],v1[VersionNumber]))). was grounded to 15 atoms.
>>> [Solving took 0.350995s]

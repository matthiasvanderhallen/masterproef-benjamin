-- Generate an idp file for the given input cudf and write it to the specified file

function run( input_filename, output_filename )
    package.path = package.path .. ";./cudf_processing/?.lua"
    package.path = package.path .. ";./cudf_processing/lupa_1.0/?.lua"

    local template_file = "dependency_resolver_v3/dependency_resolver_constants.idp.template"

    local parser = require("cudf_parser")
    local stanzas = parser.parse_cudf(input_filename)

    local preprocessor = require("cudf_preprocessor")
    stanzas = preprocessor.process(stanzas)

    local generator = require("theory_generator")
    generator.generate(stanzas, template_file, output_filename)
end

-- run("cudf_input/legacy2.cudf", "dependency_resolver_v3/dependency_resolver_legacy2.idp")
-- run("cudf_input/tmp.cudf", "dependency_resolver_v3/dependency_resolver_tmp.idp")
-- run("cudf_input/test_structures/test17.cudf", "dependency_resolver_v3/dependency_resolver_test.idp")
-- run("cudf_input/misc/misc2010/results/problems/cudf_set/small3.cudf", "dependency_resolver_v3/dependency_resolver_small3.idp")
-- run("cudf_input/test_structures/test17.cudf", "dependency_resolver_v3/dependency_resolver_test.idp")
-- run("cudf_input/misc/misc2010/results/problems/easy/rand08759d.cudf", "/tmp/tmpout.idp")
import getopt
import sys
import time
from multiprocessing import Pool
from os import listdir
from os.path import isfile, join
from subprocess import call, check_call, CalledProcessError, DEVNULL


def main():
    try:
        opts, _ = getopt.getopt(sys.argv[1:], "hd:p:o:m:t:f:v:")
    except getopt.GetoptError as err:
        print(str(err))
        sys.exit(2)

    directory = "./"
    processes = 1
    output = "cudf_output/"
    output_file = "cudf_output/output.txt"
    memory = -1
    time_limit = -1
    version = 1
    for o, a in opts:
        if o == "-h":
            print("Run the tests on cudf data. Arguments:\n" +
                  "\t-h: show this help message\n" +
                  "\t-d: directory with cudf files \tdefault: ./\n" +
                  "\t-p: number of processes to use \tdefault: 1\n" +
                  "\t-o: output directory \t\tdefault: cudf_output/\n" +
                  "\t-f: output file path \t\tdefault: cudf_output/output.txt\n" +
                  "\t-m: memory limit (MB) \t\tdefault: -1 (unlimited)\n" +
                  "\t-t: time limit (s) \t\tdefault: -1 (unlimited)\n" +
                  "\t-v: version to use (1-3) \tdefault: 1")
            sys.exit(2)
        elif o == "-d":
            directory = a
        elif o == "-p":
            processes = int(a)
        elif o == "-o":
            output = a
        elif o == "-m":
            memory = int(a)
        elif o == "-t":
            time_limit = int(a)
        elif o == "-f":
            output_file = a
        elif o == "-v":
            version = int(a)

    write("-- START --", output_file)
    files = [f for f in listdir(directory) if isfile(join(directory, f)) and f.endswith(".cudf")]

    pool = Pool(processes=processes)
    args = [(directory, f, output, output_file, memory, time_limit, version) for f in files]
    pool.map(process, args)


def process(args):
    directory, filename, output_directory, output_file, memory, time_limit, version = args
    start_time = time.time()
    write(">>> Start processing at %s: %s" % (time.strftime("%H:%M:%S", time.localtime()), filename), output_file)
    f_err = open(join(output_directory, filename + ".error"), 'w')
    f_out = open(join(output_directory, filename + ".output"), 'w')
    if version == 3:
        idp_file = "/tmp/dependency_resolver_v3_%s.idp" % filename
        repeat = True
        while repeat:
            try:
                check_call(["lua", "-e", "require('run_v3_generator'); run('%s', '%s')" %
                      (join(directory, filename), idp_file)], stdout=f_out, stderr=f_err)
                repeat = False
            except CalledProcessError as e:
                pass
        time.sleep(1)
        call(["/home/benjamin/Apps/idp/bin/idp", idp_file, "-e", "test_file('%s', '%s', %d, %d)" %
              (join(directory, filename), output_directory, memory, time_limit)], stdout=f_out, stderr=f_err)
        time.sleep(1)
        call(["rm", "-f", idp_file], stdout=DEVNULL, stderr=DEVNULL)
    else:
        test_cudf_file = "test_cudf_v1.idp"
        if version == 2:
            test_cudf_file = "test_cudf_v2.idp"
        call(["/home/benjamin/Apps/idp/bin/idp", test_cudf_file, "-e", "test_file('%s', '%s', %d, %d)" %
              (join(directory, filename), output_directory, memory, time_limit)], stdout=f_out, stderr=f_err)
    duration = time.time() - start_time
    write(">>> Finished processing after %.0fs: %s" % (duration, filename), output_file)


def write(s, output_file):
    with open(output_file, "a") as f:
        f.write(s + "\n")
    print(s)


if __name__ == "__main__":
    main()

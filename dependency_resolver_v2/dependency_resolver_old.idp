vocabulary V {
    // ==================
    // ==== Packages ====
    // ==================
    type PackageName
    type VersionNumber isa nat
    
    // A versioned package is a specific version of a package that can be installed
    type VersionedPackageType constructed from {versionedPackage(PackageName, VersionNumber)}
    type VersionedPackage isa VersionedPackageType
    
    isInstalledBefore(VersionedPackage)
    isInstalledAfter(VersionedPackage)
    
    // Predicates to indicate the changes between isInstalledBefore/1 and isInstalledAfter/1
    isRemoved(VersionedPackage)
    isNewlyInstalled(VersionedPackage)
    isUpgraded(VersionedPackage, VersionedPackage)
    
    isNotUpToDate(VersionedPackage)
    
    
    // ======================
    // ==== Dependencies ====
    // ======================
    type DependencyCount isa nat
    depends(VersionedPackage, DependencyCount, VersionedPackage)
    
    conflicts(VersionedPackage, VersionedPackage)

    
    // =======================
    // ==== Keep requests ====
    // =======================
    // The user can request to keep the version / package / feature(s) (virtual package) of a versioned package
    type KeepType constructed from {Version, Package, Feature, None}
    keep(VersionedPackage, KeepType)
    
    
    // ==================
    // ==== Requests ====
    // ==================
    type RequestType constructed from {Install, Remove, Upgrade}
    type RequestCount isa nat
    request(RequestType, RequestCount, VersionedPackage)
}

vocabulary OutputVoc {
    extern V::isInstalledBefore/1
    extern V::isInstalledAfter/1
    extern V::isRemoved/1
    extern V::isNewlyInstalled/1
    extern V::isUpgraded/2
    extern V::isNotUpToDate/1
}

theory T:V {
    // ======================
    // ==== Dependencies ====
    // ======================
    // All dependencies have to be fulfilled for the installed packages
    ! vp[VersionedPackage]: isInstalledAfter(vp) => ! n[DependencyCount]: (? vp2[VersionedPackage]: depends(vp, n, vp2)) =>
    	? vp2[VersionedPackage]: depends(vp, n, vp2) & isInstalledAfter(vp2).
    
    // For every installed package, none of conflicting packages can be installed (unless it is a self-conflict)
    ! vp[VersionedPackage]: isInstalledAfter(vp) => ! vp2[VersionedPackage]: conflicts(vp, vp2) & vp ~= vp2 
    	=> ~isInstalledAfter(vp2).
    
    
    // =======================
    // ==== Keep requests ====
    // =======================
    // Preserve the current version
    ! vp[VersionedPackage]: keep(vp, Version()) & isInstalledBefore(vp) => isInstalledAfter(vp).
    // Preserve at least one version of the package
    ! pkg[PackageName] v[VersionNumber]: keep(versionedPackage(pkg, v), Package()) 
    	& isInstalledBefore(versionedPackage(pkg, v)) 
    	=> ? v2[VersionNumber]: isInstalledAfter(versionedPackage(pkg, v2)).
    // Preserve all the provided features
    // TODO: how do this without a provides relation
    // ! vp[VersionedPackage] virt[VirtualPackage]: keep(vp, Feature()) & provides(vp, virt) & isInstalledBefore(vp) 
    //	=> ? vp2[VersionedPackage]: isInstalledAfter(vp2) & (provides(vp2, virt) | 
    //	(! pkg[PackageName] v[VersionNumber]: virt = virtualPackage(pkg, v) => provides(vp2, virtualPackage(pkg)))).
	
    
    // ==================
    // ==== Requests ====
    // ==================
    ! n[RequestCount]: (? vp[VersionedPackage]: request(Install(), n, vp)) => 
    	? vp[VersionedPackage]: request(Install(), n, vp) & isInstalledAfter(vp).
    
    ! n[RequestCount]: (? vp[VersionedPackage]: request(Remove(), n, vp)) => 
    	! vp[VersionedPackage]: request(Remove(), n, vp) => ~isInstalledAfter(vp).
    
    ! n[RequestCount]: (? vp[VersionedPackage]: request(Upgrade(), n, vp)) => 
    	?1 vp[VersionedPackage]: request(Upgrade(), n, vp) & isInstalledAfter(vp)
    	& ? pkg[PackageName] v1[VersionNumber]: vp = versionedPackage(pkg, v1)
    	& (! v2[VersionNumber]: isInstalledBefore(versionedPackage(pkg, v2)) => v1 >= v2).
    //	& (! v2[VersionNumber]: isInstalledAfter(versionedPackage(pkg, v2)) => v1 = v2). // TODO: nodig?
    
    
    // =========================
    // ==== Package changes ====
    // =========================
    {
        // A versioned package is removed if it is in isInstalledBefore/1, but not in isInstalledAfter/1 
        //     and the versioned package is not upgraded
      	! vp[VersionedPackage]: isRemoved(vp) 
        	<- isInstalledBefore(vp) & ~isInstalledAfter(vp) & ~(? vp2[VersionedPackage]: isUpgraded(vp2, vp)).
        // A versioned package is newly installed if it is not in isInstalledBefore/1, but it is in isInstalledAfter/1 
        //     and the versioned package is not upgraded
        ! vp[VersionedPackage]: isNewlyInstalled(vp) 
        	<- ~isInstalledBefore(vp) & isInstalledAfter(vp) & ~(? vp2[VersionedPackage]: isUpgraded(vp2, vp)).
        // A versioned package is upgraded to another versioned package if the first VP is in isInstalledBefore/1, 
        //     the second VP is in isInstalledAfter/1, and the version number of the second VP is strictly larger
    	! package[PackageName] v1[VersionNumber] v2[VersionNumber]: 
        	isUpgraded(versionedPackage(package, v2), versionedPackage(package, v1))
        	<- isInstalledAfter(versionedPackage(package, v1))
    		& isInstalledBefore(versionedPackage(package, v2)) & v1 > v2.
    }
    
    
    // ===============
    // ==== Other ====
    // ===============
    {
        // A versioned package is not up to date if there exists another versioned package with the same package name
        //     and a strictly larger version number
        ! v1[VersionNumber] package[PackageName]: isNotUpToDate(versionedPackage(package, v1)) <- 
        	isInstalledAfter(versionedPackage(package, v1)) &
        	? vp2[VersionedPackage] v2[VersionNumber]: vp2 = versionedPackage(package, v2) & v2 > v1.
    }
}

// Number of packages that have been removed, newly installed or upgraded
term min_removed_added_upgraded:V {
    #{vp[VersionedPackage]: isRemoved(vp) | isNewlyInstalled(vp) | ? vp2[VersionedPackage]: isUpgraded(vp2, vp)}
}

// Number of packages that are not up to date
term min_not_upgraded:V {
    #{vp[VersionedPackage]: isNotUpToDate(vp)}
}

// Paranoid: minimizing the number of packages removed in the solution, and also the packages changed by the solution (lexicographic)
term paranoid:V {
    10000 * #{vp[VersionedPackage]: isRemoved(vp)} + #{vp[VersionedPackage]: ? vp2[VersionedPackage]: isUpgraded(vp, vp2)}
}
-- Validate the MISC results: calculate the paranoid scores and 
--  compare installations

package.path = package.path .. ";../cudf_processing/?.lua"

local validator = { }
local debug = 0

-- Returns the score of the best result, my result and whether they're equal
function validator.validate( input_file, best_result_file, my_result_file ) 
    local parser = require("cudf_parser")
    local input = parser.parse_cudf(input_file)
    local best_result = parser.parse_cudf(best_result_file)
    local my_result = parser.parse_cudf(my_result_file)

    local best_score = calculate_paranoid_score(input, best_result)
    local my_score = calculate_paranoid_score(input, my_result)
    local equal = equal_results(best_result, my_result)

    if debug > 0 then
        print("score 1: " .. best_score.removed .. ", " .. best_score.changed)
        print("========")
        print("score 2: " .. my_score.removed .. ", " .. my_score.changed)
        print("========")
        print("equal: " .. tostring(equal))
    end

    return best_score, my_score, equal
end

-- Returns the score of the given result
function validator.validate_score( input_file, my_result_file ) 
    local parser = require("cudf_parser")
    local input = parser.parse_cudf(input_file)
    local my_result = parser.parse_cudf(my_result_file)

    return calculate_paranoid_score(input, my_result)
end

-- Paranoid: minimizing the number of packages removed in the solution, 
--  and also the packages changed by the solution (lexicographic)
--  lex( min #removed, min #changed) 
function calculate_paranoid_score( input, result )
    return { removed = get_nr_removed(input, result), 
             changed = get_nr_changed(input, result) }
end

--  #removed(Init,Sol) = #{ name | V(Init,name) nonempty and V(Sol,name) empty }
function get_nr_removed( input, solution )
    local p_init, v_init = get_installed_versions(input)
    local _, v_sol = get_installed_versions(solution)

    local removed = 0
    local already_removed = { }
    local s = "Removed: "
    for _, package in pairs(p_init) do
        if (v_init[package] ~= nil and v_init[package][1] ~= nil)
                and (v_sol[package] == nil or v_sol[package][1] == nil) then
            if not already_removed[package] then
                removed = removed + 1
                s = s .. package .. ", "
                already_removed[package] = true
            end
        end
    end

    if debug > 1 then print(s) end

    return removed
end

--  #changed(Init,Sol) = #{ name | V(Init,name) different to V(Sol,name) }
function get_nr_changed( input, solution )
    local package_names = { }
    
    local p_init, v_init = get_installed_versions(input)
    local p_sol, v_sol = get_installed_versions(solution)
    
    package_names = insert_all(package_names, p_init)
    package_names = insert_all(package_names, p_sol)

    local changed = 0
    local already_changed = { }
    local s = "Changed: "
    for _, package in pairs(package_names) do
        if not equal_lists(v_init[package], v_sol[package]) then
            if not already_changed[package] then
                changed = changed + 1
                s = s .. package .. ", "
                already_changed[package] = true
            end
        end
    end

    if debug > 1 then print(s) end

    return changed
end

-- Returns the set V of installed package versions
function get_installed_versions( input )
    local package_names = { }
    local v = { }

    for _, stanza in pairs(input["package"]) do
        table.insert(package_names, stanza.package)
        if stanza.installed then
            if v[stanza.package] == nil then
                v[stanza.package] = { }
            end
            table.insert(v[stanza.package], stanza.version)
        end
    end 

    return package_names, v
end

-- Checks whether the two given lists are equal
function equal_lists( list1, list2 )
    if list1 == nil and list2 == nil then
        return true
    elseif list1 == nil then
        return next(list2) == nil
    elseif list2 == nil then
        return next(list1) == nil
    end

    table.sort(list1)
    table.sort(list2)

    local keys = { }
    for i, _ in pairs(list1) do
        table.insert(keys, i)
    end
    for i, _ in pairs(list2) do
        table.insert(keys, i)
    end

    for _, v in pairs(keys) do
        if list1[v] ~= list2[v] then
            return false
        end
    end

    return true
end

-- Checks whether both results have the same packages installed
function equal_results( best_result, my_result )
    local installed = {}
    local installed_best = {}
    local installed_result = {}

    for _, stanza in pairs(best_result["package"]) do
        local package = get_versioned_package_key(stanza.package, stanza.version)
        table.insert(installed, package)
        installed_best[package] = stanza.installed
    end

    for _, stanza in pairs(my_result["package"]) do
        local package = get_versioned_package_key(stanza.package, stanza.version)
        table.insert(installed, package)
        installed_result[package] = stanza.installed
    end

    for i, package in pairs(installed) do
        if installed_best[package] ~= installed_result[package] then
            return false
        end
    end

    return true
end

-- Returns a string containing the given package name and version
function get_versioned_package_key( name, version )
    return name .. "-" .. tostring(version)
end

-- Insert all elements of the given set in the table
function insert_all( tab, set )
    for k, v in pairs(set) do
        table.insert(tab, v)
    end
    return tab
end

function to_string( key, value )
    if type(value) == "string" then
        return string.format("%s: %s", key, value)
    elseif type(value) == "number" then
        return string.format("%s: %d", key, value)
    elseif type(value) == "boolean" then
        return string.format("%s: %s", key, tostring(value))
    elseif type(value) == "table" then
        if next(value) == nil then
            return string.format("%s: []", key)
        end
        s = "["
        for i, el in pairs(value) do
            s = s .. to_string(i, el) .. ", "
        end
        s = s:sub(1, -3) .. "]"
        return string.format("%s: %s", key, s)
    else 
        return string.format("[Error] other type for %s: %s", key, type(value))
    end
end

return validator
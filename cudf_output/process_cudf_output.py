import csv
import getopt
import glob
import os
import re
import subprocess
import sys


error_extension = ".cudf.error"
output_extension = ".cudf.output"
result_extension = ".cudf.1.result"
cudf_extension = ".cudf"
output_filename = "output.txt"


def main():
    try:
        opts, _ = getopt.getopt(sys.argv[1:], "ho:d:c:r")
    except getopt.GetoptError as err:
        print(str(err))
        sys.exit(2)

    recursive = False
    directory = "./"
    cudf_directory = "./"
    csv_file = "output.csv"

    for o, a in opts:
        if o == "-h":
            print("Process the cudf test output. Arguments:\n" +
                  "\t-h: show this help message\n" +
                  "\t-o: write the result to this csv file\tdefault:output.csv\n" +
                  "\t-d: cudf output directory\t\tdefault: ./\n" +
                  "\t-c: cudf problem directory\t\tdefault: ./\n" +
                  "\t-r: recursive")
            sys.exit(2)
        elif o == "-r":
            recursive = True
        elif o == "-d":
            directory = a
        elif o == "-c":
            cudf_directory = a
        elif o == "-o":
            csv_file = a

    if recursive:
        files_pattern = "*/*" + error_extension
    else:
        files_pattern = "*" + error_extension

    path = os.path.join(directory, files_pattern)
    files = glob.glob(path)

    result = []
    for f in files:
        data = {}
        short_file_path = f.replace(directory, '').replace(error_extension, '')
        dir_path, filename = os.path.split(f.replace(error_extension, ''))

        data["file"] = short_file_path + cudf_extension
        cudf_file_path = os.path.join(cudf_directory, short_file_path + cudf_extension)
        output_file_path = os.path.join(dir_path, output_filename)
        result_file_path = os.path.join(dir_path, filename + result_extension)
        error_file_path = os.path.join(dir_path, filename + error_extension)

        with open(output_file_path) as fout:
            lines = fout.read()
            m = re.search("Finished processing after (\d+)s: %s.cudf" % filename, lines)
            data["processing_time"] = m.group(1) if m is not None else ""

        with open(error_file_path) as ferr:
            lines = ferr.read()
            m = re.search("(\d+) packages added", lines)
            data["packages"] = m.group(1) if m is not None else ""
            m = re.search("(\d+) dependencies added", lines)
            data["dependencies"] = m.group(1) if m is not None else ""
            m = re.search("Starting definition evaluation at (\d+)ms", lines)
            start_eval = int(m.group(1)) if m is not None else 0
            m = re.search("Starting approximation at (\d+)ms", lines)
            start_approx = int(m.group(1)) if m is not None else 0
            data["definition_eval"] = start_approx - start_eval if m is not None else ""
            m = re.search("Starting solving at (\d+)ms", lines)
            data["grounding"] = int(m.group(1)) - start_approx if m is not None else ""
            m = re.search("total-solving-time(\d+)ms", lines)
            data["solving"] = m.group(1) if m is not None else ""
            m = re.search("Out of resources", lines)
            data["error"] = m.group(0) if m is not None else ""

        if os.path.isfile(result_file_path):
            score = subprocess.check_output(["lua", "-e",
                                             "validator = require('validate_misc'); " +
                                             "score = validator.validate_score('%s', '%s'); " % (cudf_file_path, result_file_path) +
                                             "print('['..score.removed .. ',' .. score.changed .. ']')"])
            score = score.decode().strip()
            m = re.search("\[(\d+),(\d+)\]", score)
            data["score_removed"] = m.group(1)
            data["score_changed"] = m.group(2)

        else:
            data["score_removed"] = ""
            data["score_changed"] = ""

        print(data)
        result.append(data)

    if csv_file is not None:
        with open(csv_file, 'w') as fcsv:
            fieldnames = ["file", "packages", "dependencies", "processing_time", "definition_eval", "grounding", "solving", "score_removed", "score_changed", "error"]
            writer = csv.DictWriter(fcsv, fieldnames)
            writer.writeheader()
            for row in result:
                writer.writerow(row)


if __name__ == '__main__':
    main()

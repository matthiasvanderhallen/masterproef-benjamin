library("ggplot2")
library("dplyr")

pre_easy <- read.csv(file="data/pre/easy.csv",head=TRUE,sep=",")
pre_diff <- read.csv(file="data/pre/difficult.csv",head=TRUE,sep=",")
pre_imp <- read.csv(file="data/pre/impossible.csv",head=TRUE,sep=",")
pre_dudf <- read.csv(file="data/pre/debian-dudf.csv",head=TRUE,sep=",")
dr2_easy <- read.csv(file="data/dr2/easy.csv",head=TRUE,sep=",")
dr2_diff <- read.csv(file="data/dr2/difficult.csv",head=TRUE,sep=",")
dr2_imp <- read.csv(file="data/dr2/impossible.csv",head=TRUE,sep=",")
dr2_dudf <- read.csv(file="data/dr2/debian-dudf.csv",head=TRUE,sep=",")
nopre_synt <- read.csv(file="data/nopreprocessing.csv",head=TRUE,sep=",")
nopre_dudf <- read.csv(file="data/nopreprocessing_dudf.csv",head=TRUE,sep=",")

pre1_synt <- rbind(dr2_easy, dr2_diff, dr2_imp)
pre2_synt <- rbind(pre_easy, pre_diff, pre_imp)
pre1_dudf <- dr2_dudf
pre2_dudf <- pre_dudf

# bar chart
get_mean <- function(x) {
  return(mean(apply(x, 1, as.numeric)))
}
dat <- data.frame(
  datasets=factor(rep(c("Synthetische data", "Echte data"),3), levels=c("Synthetische data", "Echte data")),
  methode=factor(c("Voor", "Voor", "Pre1", "Pre1", "Pre2", "Pre2"),
                levels=c("Voor", "Pre1", "Pre2")),
  pakketten=c(get_mean(nopre_synt["packages"]), get_mean(nopre_dudf["packages"]),
    get_mean(pre1_synt["packages"]), get_mean(pre1_dudf["packages"]),
    get_mean(pre2_synt["packages"]), get_mean(pre2_dudf["packages"]))
)

ggplot(dat, aes(x=datasets, y=pakketten, fill=methode)) + 
  geom_bar(stat="identity", position=position_dodge(), colour="darkgray") +
  theme(text = element_text(size=20), axis.text.x = element_text(size=14)) +
  scale_fill_brewer()


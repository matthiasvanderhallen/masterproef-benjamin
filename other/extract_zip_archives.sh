#!/bin/bash
# Extract the .bz2 files in the given folder and its subfolders
# Options:
#   -d {folder} folders to extract files from
#               (default: current folder)
#   -r          recursively extract files in subfolders
#               (default: false)

recursive=false
path=$(pwd)

while getopts "rd:" opt; 
do
    case $opt in
        r) 
            recursive=true
            ;;
        d) 
            path=$OPTARG
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            exit 1
            ;;
    esac
done

if [ "$recursive" == true ]; then
    find_result=$(find $path -type f -name "*.bz2");
else
    find_result=$(find $path -maxdepth 1 -type f -name "*.bz2");
fi

for f in $find_result 
do
    $(7z x -o"$(dirname $f)" -y "$f")
    echo "Extracted $f"
    $(rm "$f")
    echo "Removed $f"
done